<?php

?>

<html>

	<head>

		<link rel="stylesheet" type="text/css" href="./assets/css/mainStyleSheet.css">
		<script src="./assets/js/jquery.js"></script>
  		<link rel="stylesheet" href="//code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css">
 	 	<script src="//code.jquery.com/ui/1.11.1/jquery-ui.js"></script>

	<script language="JavaScript" type="text/javascript">

		if (document.images) {
		    adminbuttonup       		= new Image();
		    adminbuttonup.src   		= "./assets/imgs/admin_button1.png" ;
		    adminbuttondown     		= new Image() ;
		    adminbuttondown.src 		= "./assets/imgs/admin_button2.png" ;	    

		    dubuttonup       			= new Image();
		    dubuttonup.src   			= "./assets/imgs/du_button1.png" ;
		    dubuttondown     			= new Image() ;
		    dubuttondown.src 			= "./assets/imgs/du_button2.png" ;

		    picturesbuttonup       		= new Image();
		    picturesbuttonup.src   		= "./assets/imgs/pictures_button1.png" ;
		    picturesbuttondown     		= new Image() ;
		    picturesbuttondown.src 		= "./assets/imgs/pictures_button2.png" ;

		    partsbuttonup      			= new Image();
		    partsbuttonup.src   		= "./assets/imgs/reporting_button1.png" ;
		    partsbuttondown     		= new Image() ;
		    partsbuttondown.src 		= "./assets/imgs/reporting_button2.png" ;		

		    toolboxbuttonup       		= new Image();
		    toolboxbuttonup.src   		= "./assets/imgs/toolbox_button1.png" ;
		    toolboxbuttondown     		= new Image() ;
		    toolboxbuttondown.src 		= "./assets/imgs/toolbox_button2.png" ;	    	

		    buildregistrybuttonup       = new Image();
		    buildregistrybuttonup.src   = "./assets/imgs/buildregistry_button1.png" ;
		    buildregistrybuttondown     = new Image() ;
		    buildregistrybuttondown.src = "./assets/imgs/buildregistry_button2.png" ;	

		    workflowbuttonup			= new Image();
		    workflowbuttonup.src  		= "./assets/imgs/workflow_button1.png" ;
		    workflowbuttondown			= new Image();
		    workflowbuttondown.src 		= "./assets/imgs/workflow_button2.png" ;

		    createuserbuttonup			= new Image();
		    createuserbuttonup.src  	= "./assets/imgs/createuser_button1.png" ;
		    createuserbuttondown		= new Image();
		    createuserbuttondown.src 	= "./assets/imgs/createuser_button2.png" ;	

		    edituserbuttonup			= new Image();
		    edituserbuttonup.src    	= "./assets/imgs/edituser_button1.png" ;
		    edituserbuttondown			= new Image();
		    edituserbuttondown.src  	= "./assets/imgs/edituser_button2.png" ;			    	    
		
		}
		
		function buttondown( buttonname )
		{
		    if (document.images) {
		      document[ buttonname ].src = eval( buttonname + "down.src" );
		    }
		}
		function buttonup ( buttonname )
		{
		    if (document.images) {
		      document[ buttonname ].src = eval( buttonname + "up.src" );
		    }
		}


		</script>

	</head>

	<body>

		<div id = "main_wrapper">

			<div id="default_header">

				
				
				<img src = "./assets/imgs/intraweb2_banner2.png"> 
				
			</div>

		</div>

		<div id = "default_body">

			<div id = "main_menu1" style="display:none;">

				<div id = "administration_button">
					<a href = "#" onmouseover="buttondown('adminbutton')" onmouseout="buttonup('adminbutton')">
						<img src = "./assets/imgs/admin_button1.png" name="adminbutton" border="0" />
					</a>
				</div>

				<div id = "data_utilities_button">
					<a href = "#" onmouseover="buttondown('dubutton')" onmouseout="buttonup('dubutton')">
						<img src = "./assets/imgs/du_button1.png" name="dubutton" border="0" />
					</a>
				</div>

				<div id = "pictures_button">
					<a href = "#" onmouseover="buttondown('picturesbutton')" onmouseout="buttonup('picturesbutton')">
						<img src = "./assets/imgs/pictures_button1.png" name="picturesbutton" border="0" />
					</a>
				</div>

				<div id = "parts_search_button">
					<a href = "/reporting" onmouseover="buttondown('partsbutton')" onmouseout="buttonup('partsbutton')">
						<img src = "./assets/imgs/reporting_button1.png" name="partsbutton" border="0" />
					</a>
				</div>

				<div id = "toolbox_button">
					<a href = "#" onmouseover="buttondown('toolboxbutton')" onmouseout="buttonup('toolboxbutton')">
						<img src = "./assets/imgs/toolbox_button1.png" name="toolboxbutton" border="0" />
					</a>
				</div>		

			</div>

		</div>

		<div id = "admin_menu" style="display:none;">
			
			<div id = "createuser_button">
				<a href = "#" onmouseover="buttondown('createuserbutton')" onmouseout="buttonup('createuserbutton')">
					<img src = "./assets/imgs/createuser_button1.png" name="createuserbutton" border="0" />
				</a>
			</div>	

			<div id = "edituser_button">
				<a href = "#" onmouseover="buttondown('edituserbutton')" onmouseout="buttonup('edituserbutton')">
					<img src = "./assets/imgs/edituser_button1.png" name="edituserbutton" border="0" />
				</a>
			</div>


		</div>


		<div id = "registry_menu" style="display:none;">

			<div id = "workflow_button">
				<a href = "#" onmouseover="buttondown('workflowbutton')" onmouseout="buttonup('workflowbutton')">
					<img src = "./assets/imgs/workflow_button1.png" name="workflowbutton" border="0" />
				</a>
			</div>

			<div id = "build_registry_button">
				<a href = "#" onmouseover="buttondown('buildregistrybutton')" onmouseout="buttonup('buildregistrybutton')">
					<img src = "./assets/imgs/buildregistry_button1.png" name="buildregistrybutton" border="0" />
				</a>
			</div>

		
		</div>


