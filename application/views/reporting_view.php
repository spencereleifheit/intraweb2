<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	/*

		Page Description: View for the Base Reporting Engine 
	*/

		date_default_timezone_set('America/Phoenix');

		$date = date('Y-m-d', time());

		$start_month = date('m', time());
		$start_day = date('d', time());
		$start_year = date('Y', time());

?>

	<script language="JavaScript" type="text/javascript">


		$('#main_menu1').show();


		$(document).ready(function(){

			//alert('in the jquery document ready function');

			$('#webtraffic').click(function(){
				//alert('web traffic');

				$('#content_box2').hide();
				$('#content_box1').show();
			});

			$('#customerorder').click(function(){
				//alert('customer orders');

				$('#content_box1').hide();
				$('#content_box2').show();
			});

			$('#steamingpoo').click(function(){
				alert('steaming poo!');

			});

			$('#webtrafficsubmit').click(function(){

				var start_date = $('#datepicker1').val();
				var end_date = $('#datepicker2').val();
				var keyword = $('#tags').val();

				var myData = "keyword="+keyword+"&start="+start_date+"&end="+end_date;

				//alert("DATA: " + myData);

				$.ajax({

					beforeSend: function() {

						$('#tardisnowloading').show();
						$('#tardisnowloading').append("<img src = '.assets/imgs/nowloading.gif")

					},

					url: 'http://hsp-it-01:8080/reporting/trafficReport',
					
					type: "POST",
					
					async: false,
					
					data: myData,

					cache: false,

						success: function(json) {	

							try {
								var obj = jQuery.parseJSON(json);
								//alert( "Returns: a giant object");
								//alert("ok");
							}catch(e) {
								//alert( "Returns: " + obj2['login'] + " + " + obj2['username'] + " + " + obj2['fullname']);
								alert('Exception while request..');	
							}
						},

						error: function() {

							//alert( "Returns: " + obj2['login'] + " + " + obj2['username'] + " + " + obj2['fullname']);
							alert('Error while request..');
						}					

				});

				alert('done');
				$('#tardisnowloading').hide();

				return false;

			});

		});

	</script>

	<script>
  		
  		$(function() {
    		$( "#datepicker1" ).datepicker({
    			changeMonth: true,
    			changeYear: true,
    			yearRange: '2013:2014',
    			defaultDate: 'today'
    		});
  		});

  		$(function() {
    		$( "#datepicker2" ).datepicker({
    			changeMonth: true,
    			changeYear: true,
    			yearRange: '2013:2014',
    			maxDate: 'today'

    		});
  		});
  		
  		$(function() {

  			var availableTags = [
  				"Batman",
  				"calculateShipping",
  				"checkout",
  				"discount",
  				"document",
  				"downloads",
  				"partsology",
  				"pink flamingo",
  				"login",
  				"tutorial"
  			];

  			$("#tags").autocomplete({
  				source: availableTags
  			});

  		});

	</script>


	<style type = "text/css">

	#reportingbodybag {
		font-size: 14px;
		font-family: verdana;

		position: absolute;
		top: 240px;
		left: 30px;

		width: 960px;
		height: 600px;	

	}

	#reportingmenu {
		position: absolute;
		font-size: 16px;
		font-family: verdana;
		top: 300px;
		left:50px;
		width: 200px;
		height: 300px;
		z-index: 9;
	}

	</style>
 
	<div id = "reportingmenu">
		
		<a href = "#" id="webtraffic">
		Web Traffic Reports
		</a>

		<br/><br/><br/>

		<a href = "#" id="customerorder">
		Customer Order Reports
		</a>

		<br/><br/><br/>

		<a href = "#" id="steamingpoo">
		Menu Choice 3
		</a>

		<br/><br/><br/>

	</div>

	<div id = "reportingbodybag">

		<span class = "head1">
			<img src = "./assets/imgs/reporting_registry_header.png">
		</span>

	</div>

	<div id = "content_box1">

		<span class = "head1">
			Website Traffic Report Builder
		</span>

		<br/>
		<br/>
		This report searches the Website Activity Log and quantifies Page Visits based upon the
		the keyword you provide
		<br/>
		<br/>

		<form name="web_traffic_form" id="web_traffic_form" method="POST" action="#">

			<table width="500" border="0" cellpadding="0" cekllspacing="0">

				<tr>
					<td width="170">
						<strong>Keyword:</strong>
					</td>
					<td width="230">
						<div class = "ui-widget">
							<input type="textbox" id="tags" size="30">
						</div>
					</td>
				</tr>

				<tr>
					<td colspan="2" height="30">
						<br/>
					</td>
				</tr>

				<tr>
					<td>	
						<strong>Start Date:</strong>
					</td>
					<td>
						<input type="textbox" id="datepicker1">
					</td>
				</tr>

				<tr>
					<td>
						<strong>End Date (optional):</strong>
					</td>
					<td>
						<input type="textbox" id="datepicker2">
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
					<br/>
					</td>
				</tr>
				<tr>
					<td colspan="2" height="30">
					<br/>
					</td>
				</tr>
				<tr>
					<td colspan="2" align="center">
						<input type="submit" id="webtrafficsubmit" value="Let's Run This Bad Boy!">
					</td>
				</tr>

			</table>
		</form>

	</div>

	<div id = "content_box2">

		<span class = "head1">
			Customer Order Report Builder
		</span>

		<br/>
		<br/>

	</div>

	<div id="tardisnowloading">
		<img src="./assets/imgs/nowloading.gif" width="120" height="160">
		<br/>
		<span class= "pooey" style="color:blue">
			<strong>Now Loading...</strong>
		</span>
	</div>
