<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	/*

		Page Description: View for Image Registry

	*/
		date_default_timezone_set('America/Phoenix');

		$date = date('Y-m-d', time());

		$start_month = date('m', time());
		$start_day = date('d', time());
		$start_year = date('Y', time());
?>


	<script language="JavaScript" type="text/javascript">

			$('#main_menu1').show();

	</script>

	<style type = "text/css">

	#irbodybag {
		font-size: 14px;
		font-family: verdana;

		position: absolute;
		top: 240px;
		left: 30px;

		width: 960px;
		height: 600px;	

	}

	#irmenu {
		position: absolute;
		font-size: 16px;
		font-family: verdana;
		top: 300px;
		left:50px;
		width: 200px;
		height: 300px;
		z-index: 9;
	}


	</style>

	<div id = "irmenu">

		<a href = "/build_registry">
		Build/Rebuild Registry
		</a>

		<br/><br/><br/>
		
		<a href = "#">
		View Registry
		</a>

		<br/><br/><br/>

		<a href = "#">
		Manage Registry
		</a>



		<br/><br/><br/>


	</div>




	<div id = "irbodybag">

		<span class = "head1">
			<img src = "./assets/imgs/image_registry_header.png">
		</span>

	</div>


