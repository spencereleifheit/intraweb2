<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

set_time_limit(0);

class Amp_test2 extends CI_Controller{

    var $helpers = array(
        );

    var $url = "http://HSP-TEST-UPS/amp/wsdl";

    var $services = array("CONNECTSHIP_UPS.UPS.GND", "CONNECTSHIP_UPS.UPS.2DA");

    var $soap = array("soap_version" => SOAP_1_2);

    var $client; 

    public function __construct(){

        parent::__construct();
        // foreach ($this->helpers as $helper) {

        //  $this->load->helper($helper);
        // }

    }

    public function makeSoap() {

        $this->client = new SoapClient($this->url, $this->soap);

        echo "Made SOAP connection to AMP API... <br/>";
    }

    public function DontDropTheSoap($services, $defaults, $packages, $sortType ) {

        $req = array($services, $defaults, $packages, $sortType);

        $response = $this->client->Rate($req);  

        if (!$response) {
            die ("request failed");
        }

        else {

            return true;

        }

    }

    public function index(){
        // create client
        $client = new SoapClient($this->url, array("soap_version" => SOAP_1_2));

        // services to ship
        $service = "TANDATA_UPS.UPS.GND";

        // create defaults as empty array and populate using [] syntax
        $defaults = array();
        $defaults["shipper"] = "TEX";
        $defaults["packaging"] = "CUSTOM";
        $defaults["shipdate"] = "";

        // create NameAddress complex type as array
        $consignee = array("company" => "Test Co.", "contact" => "Test Contact", 
                "address1" => "1234 West Dr.", "city" => "Austin",
                "stateProvice" => "TX", "postalCode" => "10012",
                "countryCode" => "US");

        $defaults["consignee"] = $consignee;

        // create empty packages list
        $packages = array();

        // create new package dictionary
        $package = array();
        $package["weight"] = array("value" => "2.3 lbs");

        // add package to list
        $packages[] = $package;

        // create 2nd package dictionary
        $package = array();
        $package["weight"] = array("value" => "22.7 lbs");
        $package["additionalHandling"] = true;

        // add 2nd package to list
        $packages[] = $package;

        // create ShipRequest message
        $shipreq = array("service" => $service, "defaults" => $defaults, "packages" => $packages);

        // document to print
        $document = "TANDATA_UPS_MAXICODE_US_DOMESTIC.STANDARD";

        // output type
        $output = "png";

        // stock to use
        $stock = array("symbol" => "THERMAL_LABEL_8");

        // create PrintRequest message
        $printreq = array("document" => $document, "output" => $output, "stock" => $stock);

        // create two SoapVars for the ship and print requests
        // these have to have specific schema types and element namespaces for the PHP serializer to work correctly for compound request array 
        $ampns = "urn:connectship-com:ampcore";
        $shipvar = new SoapVar($shipreq, SOAP_ENC_OBJECT, "ShipRequest", $ampns, "shipRequest", $ampns);
        $printvar = new SoapVar($printreq, SOAP_ENC_OBJECT, "PrintRequest", $ampns, "printRequest", $ampns);

        // create compound ship + print
        $compreq = array($shipvar, $printvar);

        // invoke compound operation
        // have to use another SoapVar here to ensure the correct the ship+print ordering
        $resp = $client->Compound(new SoapVar($compreq, SOAP_ENC_OBJECT));

        // process ship result
        $result = $resp->shipResponse->result;

        var_dump($result);
        // print "Ship return code: ".$result->code."\n";
        // print "Ship return message: ".$result->message."\n";

        // // display rate and transit time
        // print "  Total: ".$result->resultData->total->value."\n";
        // print "  Commitment: ".$result->packageResults->item[0]->resultData->timeInTransit->name."\n";

        // print "\n";

        // // handle returned label images
        // $result = $resp->printResponse->result;
        // print "Print return code: ".$result->code."\n";
        // print "Print return message: ".$result->message."\n";
        // foreach ($result->resultData->item as $i) {
        //     // base64 image data is automatically decoded by SoapClient
        //     $image = $i->output->imageList->imageOutput;

        //     // write data to file, named w/ MSN
        //     $fn = $i->sourceItem->msn.".png";
        //     $file = fopen($fn, "wb");
        //     fwrite($file, $image);
        //     fclose($file);

        //     // print image info
        //     $info = getimagesize($fn);
        //     print "Image info for ".$fn.": ".$info[3]."\n"; 
        // }
    }

}