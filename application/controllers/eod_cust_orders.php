<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');



class Eod_cust_orders extends CI_Controller {

	var $dbconn;

	var $ftpconn;

	var $customers = array(
		"12679",
		"900382",
		"9014103",
		"C10430"
		);

	var $customer;

	var $helpers = array(
		"sqldata", 
		"previewftp"
		);

	public function __construct(){

		parent::__construct();

		// Load all Database Connector Helpers
		foreach ($this->helpers as $helper) {

			$this->load->helper($helper);
		}

	}


	public function index() {


		$this->dbconn = sqldataConnect('data01hsp');

		foreach ($this->customers as $customer){

			$this->customer = $customer;

			$this->runQuery();

		}

		echo "successfull run";

	}


	public function runQuery(){

		date_default_timezone_set('America/Phoenix');

		$date = date('Y-m-d', time());

		$time_append = " 00:00:00.000";

		$five_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 5, date("Y")));

		$four_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 4, date("Y")));	

		$three_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 3, date("Y")));

		$two_days_ago = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 2, date("Y")));

		$yesterday = date('Y-m-d', mktime(0, 0, 0, date("m") , date("d") - 1, date("Y")));

		$date2 = $yesterday;

		$five_days_ago = $five_days_ago.$time_append;

		$four_days_ago = $four_days_ago.$time_append;

		$three_days_ago = $three_days_ago.$time_append;

		$two_days = $two_days_ago;

		$two_days_ago = $two_days_ago.$time_append;

		$yesterday = $yesterday.$time_append;

		$today = $date.$time_append;

		$customer = $this->customer;
	
		$file_name = $this->customer."_".$date2.".csv";

		$handle = fopen($file_name, "a");

		//echo $today."<br/>";

		//$handle = fopen('php://output', "a");

		fputcsv($handle, array(
			'Customer',
			'Invoice No',
			'Our Order No',
			'Order Date',
			'Your PO Number',
			'Our Web Reference',
			//'artype',
			//'arstat',
			//'frt_url',
			'Carrier Tracking'
			));

		$query = "SELECT

			A.custno AS custno,
			A.invno AS invno,
			B.sono AS sono,
			B.ordate AS ordate,
			B.ponum AS ponum,
			B.sodate AS sodate,
			B.websono AS websono,
			A.artype AS artype,
			A.arstat AS arstat,
			A.frt_track AS frt_track
							

			FROM dbo.SOMAST as B

				INNER JOIN dbo.ARMAST AS A
							
					ON (A.ornum = B.sono)
								
			WHERE (B.custno = '".$customer."')
									
				AND
									
					(A. artype = '')
									
				AND
									
					(A.arstat = '')
									
				AND
									
					(B.ordate > = '".$yesterday."')
									
									
			ORDER BY B.custno, B.ordate";


			//echo "<h3>".$customer."</h3>";

		$stmt = sqlsrv_query($this->dbconn, $query);

		$results0 = 0;
			
		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

			$custno = $row['custno'];
			$invno = $row['invno'];
			$sono = $row['sono'];
			//$ordate = $today;
			$ponum = $row['ponum'];
			$websono = $row['websono'];
			$artype = $row['artype'];
			$arstat = $row['arstat'];
			$frt_track = $row['frt_track'];

			$dookie = array($custno, $invno, $sono, $date2, $ponum, $websono, $frt_track);

			fputcsv($handle, $dookie);


			// echo $row['custno'].", ".$row['invno'].", ".$row['sono'].", ".$two_days_ago.", ".$row['websono'].", ";
			// echo $row['artype'].", ".$row['arstat'].", ";
			// echo "<a href = '".$row['frt_track']."'>click here</a>";
			// echo $row['frt_track']."\n";
				
			$results0++;
			}

				
		fclose($handle);
		//echo "<h3>".$results0." results</h3><br/><br/>";	

		$conn_str = "preview.horizonparts.com";   // <~~ FTP Connection Stream
		$login = "hspp1-3apreview";  // <~~ FTP Username
		$password = "Gold4444!";  // <~~ FTP Password


		// Test basic connection to FTP Server, as in .. Can we even get there?
		$conn_id = ftp_connect($conn_str) or die("Couldn't connect to $ftp_server");					
		
		ftp_login($conn_id, $login, $password) or die("Login not successful");

		ftp_put($conn_id, '/eod_customer_csv/'.$file_name, $file_name, FTP_ASCII);

		ftp_close($conn_id);

		unlink($file_name);

	}

}