<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

set_time_limit(0);


class Discounted_report extends CI_Controller {

	// Array to hold every DB that is connected to and load the Appropriate Helper Function
	var $helpers = array(
		//"sqldata", 
		//"sqldata2", 
		"livesite",
		"lastsevendays"
		//"devo2", 
		//"devo3", 
		//"intraweb", 
		//"intraweb2",
		//"previewftp"
		);

	var $dbconn;

	public function __construct(){

		parent::__construct();

		// Load all Database Connector Helpers
		foreach ($this->helpers as $helper) {

			$this->load->helper($helper);
		}

	}


	public function index() {

		$this->dbconn = liveSiteDBConnect();
		$this->load->view('discounted_report_view');
		$this->quantifyDiscountedItems();

	}

	public function quantifyDiscountedItems() {

		$lastfive = goGetTheLastSevenDays();

		$beginTime = ' 00:00:00';
		$endTime = ' 23:59:59';

		$begin = $lastfive[1].$beginTime;
		$end = $lastfive[1].$endTime;

		$bigAssQuery = "SELECT 

						SessionId,
						
						UserId,
						
						CustNo,
						
						IPAddress,
						
						LogTime,
						
						ActivityType,
						
						Referrer,
						
						PageName

						FROM dbo.SiteActivityLOg

						WHERE

							(

							LogTime >= '".$begin."'

							AND 

							LogTime <= '".$end."'

							)

							AND

							RequestURL LIKE '%discount%'

							AND 

							(

							IPAddress != '216.64.152.77'

							AND

							IPAddress != '70.90.171.40'
							
							AND
							
							IPAddress != '70.90.171.41'
							
							AND
							
							IPAddress != '70.90.171.42'
							
							AND
							
							IPAddress != '70.90.171.43'
							
							AND
							
							IPAddress != '70.90.171.44'
							
							AND
							
							IPAddress != '70.90.171.45'
							
							AND
							
							IPAddress != '70.90.171.46'
							
							AND
							
							IPAddress != '70.90.171.47'

							)

						ORDER BY LogTime ASC
							";

		$file_name = "\\\\Intraweb\\share\\wamp\\www\\discounted_items_reports\\discounted".$lastfive[1].".csv";

		$handle = fopen($file_name, "a");

		// DEBUG -------------------------------------------------
		var_dump($lastfive);
		echo "<br/><br/>";

		fputcsv($handle, array(
			'SessionId',
			'UserId',
			'CustNo',
			'IPAddress',
			'LogDate',
			'ActivityType',
			'Referrer',
			'PageName'
			));

		$results = 0;

		$stmt = sqlsrv_query($this->dbconn, $bigAssQuery);

		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

			$sessionid = $row['SessionId'];
			$userid = $row['UserId'];
			$custno = $row['CustNo'];
			$ipaddress = $row['IPAddress'];
			$logtime = $row['LogTime'];
			$activitytype = $row['ActivityType'];
			$referrer = $row['Referrer'];
			$pagename = $row['PageName'];

			$dookie = array($sessionid, $userid, $custno, $ipaddress, $lastfive[1], $activitytype, $referrer, $pagename);

			var_dump($dookie);
			echo "<br/>";

			fputcsv($handle, $dookie);

			$results++;

		}

		echo $results." result(s) found";


	}



}

