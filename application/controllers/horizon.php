<?php


class Horizon extends CI_Controller {


	public function index() {

		session_start();

		$this->load->helper('url');
		
		$this->load->database();

		$this->load->view("default_header");

		$this->load->view('default_navbar');

		$this->load->view('horizon_landing_page');

		$this->load->view("default_footer");

	}
	


}