<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

set_time_limit(0);


class Build_registry extends CI_Controller {

	/* Array to handle the Database Connection Objects
			Correct Usage -- array_push($this->dbconns, <Database Connection Function>)
				Database Connection Functions Listed as follows:
				================================================

					sqldata 	: sqldataConnect('<database name>')
					sqldata2 	: sqldata2Connect('<database name>')
					devo2 		: devo2Connect()
					devo3    	: devo3Connect()
					intraweb    : intrawebConnect()
					intraweb2 	: intraweb2Connect()
	*/
	var $dbconns = array();

	var $ftpconns = array();

	// Array to handle all ietm numbers read from DATA01HSP Database
	var $item_list = array();

	// Array to hold every DB that is connected to and load the Appropriate Helper Function
	var $helpers = array(
		"sqldata", 
		"sqldata2", 
		"livesite", 
		"devo2", 
		"devo3", 
		"intraweb", 
		"intraweb2",
		"previewftp"
		);


	// This object reads in a Tewmporary Array from DATA01HSP (item #) and RBEACON (Bin Location)
	var $item_registry = [
		"item" => null,
		"filename" => null,
		"extension" => null,
		"path" => null,
		"live" => false,
		'local' => false
	];


	public function __construct(){

		parent::__construct();

		// Load all Database Connector Helpers
		foreach ($this->helpers as $helper) {

			$this->load->helper($helper);
		}

	}

	public function establishDbConnections(){

		$this->dbconns[0] = sqldataConnect('data01hsp');
		$this->dbconns[1] = sqldataConnect('image_registry');
		$this->dbconns[2] = sqldataConnect('rbeacon');

	}

	public function establishFTPConnections(){
		$this->ftpconns[0] = previewftpConnect();
	}


	public function index() {

		// Establish Needed Database Connections
		$this->establishDbConnections();
		//debug
		var_dump($this->dbconns);

		$this->load->view('data_utilities_menu');

		$this->load->view('build_registry_view');

		//$this->readInventoryItems($this->dbconns[0], $this->dbconns[1], $this->dbconns[2]);

		$this->establishFTPConnections();

		var_dump($this->ftpconns);

		$this->parseFTP($this->ftpconns[0]);

	}


	/*	Massive Query to PRO's icitem table to determine active items that have not been replaced and are kept in stock
	    	and are marked as sellable on the live website
	*/ 

	public function readInventoryItems($connection, $connection2, $connection3){

		$conn = $connection;
		$conn2 = $connection2;
		$conn3 = $connection3;


		$clear = "DELETE FROM dbo.items WHERE (1 = 1)";

		$stmt0 = sqlsrv_query($conn2, $clear);

		if ($stmt0 == false) {
			die( print_r( sqlsrv_errors(), true));
		}


		$query = "SELECT 
						A.item AS item, 
		 				A.ionhand AS onhand,
		 				A.esellable AS sellable,
						A.webtype AS webtype,
						A.eimagesm AS imagesm
		 		
		 			FROM dbo.icitem AS A


					WHERE
						(
							A.ionhand > '0'
						) 

					AND

						(
							(A.webtype != 'NLA')
						AND
							(A.webtype != 'RPL')
						)
					AND
						(
				
							(A.item LIKE '__-___-____%')
						
						OR

							(left(A.item,1) = '_')
					
						)

						
					ORDER BY A.item ASC";

		$stmt = sqlsrv_query($conn, $query);

		if ($stmt == false) {
			die( print_r( sqlsrv_errors(), true));
		}

		$previous = null;
		$results0 = 0;
		while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {

			$tag = 0;

			$test = substr($row['item'],7,1);
			

			if ($test == 'w' || $test == 'W'){
				//echo "W found! - ";
				$test2 = substr($row['item'], 11,1);

				if ($test2 == 'k' || $test2 == 'K'){
					//echo "K found! , omitting result. <br/>";
					$tag = 1;
				}

				if ($test2 == 'a' || $test2 == 'A'){
					//echo "A found! , omitting result. <br/>";
					$tag = 1;
				}

				if ($test2 == 'd' || $test2 == 'D'){
					//echo "A found! , omitting result. <br/>";
					$tag = 1;
				}

			}

			/* Refurbished items are a special case where they may or may not be associated with active items
					The only way to check for the item number's validity is to compare it against the item
					right before it. If the base item numbers are the same then the 'R' item is also valid
			*/

			$test3 = substr($row['item'], 11, 1);
				if ($test3 == 'r' || $test3 == 'R'){

					if ($previous == (substr($row['item'],0,11))){
						//echo "R found!, ";
						//$tag = 1;
					}

					else {
						//echo "R found!, Not associated with an active item - omitted!<br/>";
						$tag = 1;
					}
				}

				if ($test3 == 'm' || $test3 == 'M'){
					//echo "M found!, omitting result. <br/>";
					$tag = 1;
				}


			if ($tag == 0) {

				$binlabel = null;
				$bintype = null;
				$binzone = null;

				$inner_query = "
								SELECT 
								
									BINLABEL AS binlabel, PRODUCT, ZONE AS binzone, BINTYPE AS bintype 
								
								FROM dbo.BINLOCAT 
									
									WHERE PRODUCT = '".$row['item']."'";

				$inner_stmt = sqlsrv_query($conn3, $inner_query);

					while($row2 = sqlsrv_fetch_array( $inner_stmt, SQLSRV_FETCH_ASSOC)) {
						$binlabel = $row2['binlabel'];
						$bintype = $row2['bintype'];
						$binzone = $row2['binzone'];
					}

				echo $row['item']."<br/>";
				$previous = substr($row['item'], 0, 11);
				$results0++;

				$query2 = "INSERT INTO dbo.items (item, onhand, sellable, webtype, imagesm, binlabel, binzone, bintype) VALUES ('".$row['item']."', '".$row['onhand']."', '".$row['sellable']."', '".$row['webtype']."', '".$row['imagesm']."', '".$binlabel."', '".$binzone."', '".$bintype."')";

				$stmt2 = sqlsrv_query($conn2, $query2);


				if ($stmt2 == false) {
					die( print_r( sqlsrv_errors(), true));
				}

				echo "Record written properly";
			}
		}

		echo $results0." Items found matching search<br/><br/>";



	}

	public function parseFTP($conn) {

		$clear = "DELETE FROM dbo.ftp WHERE (1 = 1)";

		$stmt0 = sqlsrv_query($this->dbconns[1], $clear);

		if ($stmt0 == false) {
			die( print_r( sqlsrv_errors(), true));
		}

		$dookie = ftp_nlist($conn, '/images_mapped');

		$count = 0;

		$shortname = null;

		foreach ($dookie as $poo) {

			$ext = pathinfo($poo, PATHINFO_EXTENSION);
			$file = pathinfo($poo, PATHINFO_FILENAME);
			$fname = substr(pathinfo($poo, PATHINFO_BASENAME),0,1);

			if ($fname >= '0' && $fname <='9' ) {


				$f1 = substr($file, 7,1);

				$shortname = substr($file,0,10);
				$parenpos1 = strpos($file, "(");
				$parenpos2 = strpos($file, ")");
				$uspos = strpos($file, "_");	


				if ($f1 == 'w' || $f1 == 'W'){
					$shortname = substr($file,0,11);
				}
				
				$word = null;

				if (($ext == 'jpg' || $ext == 'JPG' || $ext == 'png' || $ext == 'PNG')){

					if ($parenpos1 && $parenpos2) {

						$word = substr($file, ($parenpos1+1), ($parenpos2-1));
					}

					//echo $fname."- ".$word." - ".$uspos." ---- ";

					//echo $shortname.", ".pathinfo($poo, PATHINFO_EXTENSION)."<br/>";

				$shortname = (string)$shortname;
				
				$long_name = pathinfo($poo, PATHINFO_FILENAME);
				$long_name = (string)$long_name;

				$extension = pathinfo($poo, PATHINFO_EXTENSION);
				$extension = (string)$extension;

				$full_name = pathinfo($poo, PATHINFO_BASENAME);
				$full_name = (string)$full_name;

				//echo $shortname."<br/>";	

				// $query3 = "INSERT INTO dbo.ftp (filename, extension, fullname, item) VALUES ('".$long_name."', '".$extension."', '".$full_name."', '".$shortname."')";

				// $stmt3 = sqlsrv_query($this->dbconns[1], $query3);


				// if ($stmt3 == false) {
				// 	die( print_r( sqlsrv_errors(), true));
				// }



					//$count++;

				}
			}

		}

		echo "<h3>next.............................</h3>";

		$dookie = ftp_nlist($conn, '/images_content');

		foreach ($dookie as $poo) { 										

			$ext = pathinfo($poo, PATHINFO_EXTENSION);						
			$file = pathinfo($poo, PATHINFO_FILENAME);
			$path = pathinfo($poo, PATHINFO_DIRNAME);
			$fname = substr(pathinfo($poo, PATHINFO_BASENAME),0,1);	

			//$mdate = date("F d Y H:i:s.", filemtime($file.".".$ext));		variant

			if ($fname >= '0' && $fname <= '9' ) {										

				$f1 = substr($file, 7,1);

				$shortname = substr($file,0,10);
				$parenpos1 = strpos($file, "(");
				$parenpos2 = strpos($file, ")");
				$uspos = strpos($file, "_");


				if ($f1 == 'w' || $f1 == 'W'){
					$shortname = substr($file,0,11);
				}

				$word = null;

				$size = null;

				$part = $file;

				if ($uspos) {
					$size = substr($file, $uspos+1, 2);
					//echo $size.' - ';

					$part = substr($file, 0 , $uspos);

				}


				if ($ext == 'jpg' || $ext == 'JPG' || $ext == 'png' || $ext == 'PNG'){

					if ($parenpos1 && $parenpos2) {

						$word = substr($file, ($parenpos1+1), (($parenpos2 - 1) - ($parenpos1)));

						$part = substr($file, 0, $parenpos1);

						//echo $fname."- ".$word." - ";
					}		

					//echo $part."---- ";	

					//echo pathinfo($poo, PATHINFO_BASENAME).", ".pathinfo($poo, PATHINFO_EXTENSION)."<br/>";		

				$shortname = (string)$part;
				
				$long_name = pathinfo($poo, PATHINFO_FILENAME);
				$long_name = (string)$long_name;

				$extension = pathinfo($poo, PATHINFO_EXTENSION);
				$extension = (string)$extension;

				$full_name = pathinfo($poo, PATHINFO_BASENAME);
				$full_name = (string)$full_name;

				echo $long_name."<br/>";

				// $query3 = "INSERT INTO dbo.ftp (filename, extension, fullname, item) VALUES ('".$long_name."', '".$extension."', '".$full_name."', '".$shortname."')";

				// $stmt3 = sqlsrv_query($this->dbconns[1], $query3);


				// if ($stmt3 == false) {
				// 	die( print_r( sqlsrv_errors(), true));
				// }
					//echo $shortname."<br/>";


					//$count++;												

				}
			}									

		}
		
		echo "<h3>next.............................</h3>";

		$dookie = ftp_nlist($conn, '/images_product');

		foreach ($dookie as $poo) { 										

			$ext = pathinfo($poo, PATHINFO_EXTENSION);						
			$file = pathinfo($poo, PATHINFO_FILENAME);
			$path = pathinfo($poo, PATHINFO_DIRNAME);
			$fname = substr(pathinfo($poo, PATHINFO_BASENAME),0,1);	

			//$mdate = date("F d Y H:i:s.", filemtime($file.".".$ext));		variant

			if ($fname >= '0' && $fname <= '9' ) {										

				$f1 = substr($file, 7,1);

				$shortname = substr($file,0,10);
				$parenpos1 = strpos($file, "(");
				$parenpos2 = strpos($file, ")");
				$uspos = strpos($file, "_");


				if ($f1 == 'w' || $f1 == 'W'){
					$shortname = substr($file,0,11);
				}

				$word = null;

				$size = null;

				$part = $file;

				if ($uspos) {
					$size = substr($file, $uspos+1, 2);
					//echo $size.' - ';

					$part = substr($file, 0 , $uspos);

				}


				if ($ext == 'jpg' || $ext == 'JPG' || $ext == 'png' || $ext == 'PNG'){

					if ($parenpos1 && $parenpos2) {

						$word = substr($file, ($parenpos1+1), (($parenpos2 - 1) - ($parenpos1)));

						$part = substr($file, 0, $parenpos1);

					}		

					//echo $part."---- ";	

					//echo pathinfo($poo, PATHINFO_BASENAME).", ".pathinfo($poo, PATHINFO_EXTENSION)."<br/>";		

				$shortname = (string)$part;
				
				$long_name = pathinfo($poo, PATHINFO_FILENAME);
				$long_name = (string)$long_name;

				$extension = pathinfo($poo, PATHINFO_EXTENSION);
				$extension = (string)$extension;

				$full_name = pathinfo($poo, PATHINFO_BASENAME);
				$full_name = (string)$full_name;

				echo $long_name."<br/>";

				$query3 = "INSERT INTO dbo.ftp (filename, extension, fullname, item) VALUES ('".$long_name."', '".$extension."', '".$full_name."', '".$shortname."')";

				$stmt3 = sqlsrv_query($this->dbconns[1], $query3);



				if ($stmt3 == false) {
					die( print_r( sqlsrv_errors(), true));
				}
				


					$count++;												

				}
			}									

		}
		

		echo $count." qualifying image files found. <br/><br/>";
	}


	public function parseLocalFolders($dir) {


	}






}