<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(E_ALL);

set_time_limit(0);

class Amp_test extends CI_Controller{

	var $helpers = array(
		);

	var $url = "http://hsp-test-ups:8080/wcfShip";

	var $headers = array();

	var $curl;


	/* Services That cen be passed

	TANDATA_UPS.UPS.NAM
	TANDATA_UPS.UPS.NDA
	TANDATA_UPS.UPS.NDS
	TANDATA_UPS.UPS.2AM
	TANDATA_UPS.UPS.2DA
	TANDATA_UPS.UPS.3DA
	TANDATA_UPS.UPS.GND
	TANDATA_UPS.UPS.WEXPPLS
	TANDATA_UPS.UPS.WEXP
	TANDATA_UPS.UPS.WEPD
	TANDATA_UPS.UPS.WSTD
	TANDATA_UPS.UPS.WEXPSVR

	CONNECTSHIP_ENDICIA.USPS.FIRST
	CONNECTSHIP_ENDICIA.USPS.PRIORITY
	
	CONNECTSHIP_ENDICIA.USPS.EXPR
	CONNECTSHIP_ENDICIA.USPS.MEDIA
	
	CONNECTSHIP_ENDICIA.USPS.PARCELPOST
	CONNECTSHIP_ENDICIA.USPS.I_FIRST
	CONNECTSHIP_ENDICIA.USPS.I_PRIORITY
	CONNECTSHIP_ENDICIA.USPS.I_EXPR
	
	CONNECTSHIP_ENDICIA.USPS.CRITICAL

	*/	


/*	<?xml version="1.0" encoding="utf-8"?> */

	var $xml = '
			<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:psi="http://PSI.Wcf">
			   <soapenv:Header/>
			   <soapenv:Body>
			      <psi:RateServices>
			         <psi:request>
			            <psi:def_attr>
			               <psi:consignee>
			                  <psi:address1>240 9th St</psi:address1>
			                  <psi:city>Marathon</psi:city>
			                  <psi:company>UPS</psi:company>
			                  <psi:contact>JOE UPSER</psi:contact>
			                  <psi:country_symbol>UNITED_STATES</psi:country_symbol>
			                  <psi:postal_code>33050</psi:postal_code>
			                  <psi:state_province>FL</psi:state_province>
			               </psi:consignee>
			               <psi:shipper>HSPP</psi:shipper>
			               <psi:shipdate>2014-09-19</psi:shipdate>
			               </psi:def_attr>
			            <psi:packages>
			               <psi:PackageRequest>
			                  <psi:weight>.5</psi:weight>
			               </psi:PackageRequest>
			            </psi:packages>
			            <psi:printRequest/>
			         </psi:request>
			         <psi:services>

			            <psi:string>CONNECTSHIP_UPS.UPS.GND</psi:string>
			            <psi:string>CONNECTSHIP_UPS.UPS.NDA</psi:string>
			            <psi:string>CONNECTSHIP_UPS.UPS.NAM</psi:string>
			            <psi:string>CONNECTSHIP_UPS.UPS.2DA</psi:string>
			            <psi:string>CONNECTSHIP_UPS.UPS.2AM</psi:string>
			            <psi:string>CONNECTSHIP_UPS.UPS.3DS</psi:string>


						<psi:string>CONNECTSHIP_ENDICIA.USPS.FIRST</psi:string>

						<psi:string>CONNECTSHIP_ENDICIA.USPS.PRIORITY</psi:string>

			          </psi:services>
			         <psi:sortType>1</psi:sortType>
			         <psi:Params/>
			      </psi:RateServices>
			   </soapenv:Body>
			</soapenv:Envelope>
	';

	var $soap = array("soap_version" => SOAP_1_2);

	var $client;

	var $server; 

	public function __construct(){

		parent::__construct();

	}

	public function makeCurl($bigXmlString){

		 //setting the curl parameters.
	 	$this->headers = array(
		    "Content-type: text/xml;charset=\"utf-8\"",
		    "Accept: text/xml",
		    "Cache-Control: no-cache",
		    "Pragma: no-cache",
		    "SOAPAction: http://PSI.Wcf/IwcfShip/RateServices",
		    //"Content-length: ".strlen($this->xml),
		 );

		try {

			$this->curl = curl_init();

			curl_setopt_array(
				$this->curl, array(
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_VERBOSE => true,
				CURLOPT_URL => $this->url,
				CURLOPT_POST => true,
				CURLOPT_POSTFIELDS => $this->xml,
				CURLOPT_HTTPHEADER => $this->headers
			));

			$data = curl_exec($this->curl);

			//echo "Error: ";

			//printf (curl_errno($this->curl), htmlspecialchars(curl_error($this->curl)));

			echo $data;

			curl_close($this->curl);

		}catch(Exception $e){
			
			echo "Error is: ".$e->getMessage();die("Error");
		
		}


	}

	public function index(){

		$this->makeCurl($this->xml);

	}



}



	/* Services That cen be passed

	TANDATA_UPS.UPS.NAM
	TANDATA_UPS.UPS.NDA
	TANDATA_UPS.UPS.NDS
	TANDATA_UPS.UPS.2AM
	TANDATA_UPS.UPS.2DA
	TANDATA_UPS.UPS.3DA
	TANDATA_UPS.UPS.GND
	TANDATA_UPS.UPS.WEXPPLS
	TANDATA_UPS.UPS.WEXP
	TANDATA_UPS.UPS.WEPD
	TANDATA_UPS.UPS.WSTD
	TANDATA_UPS.UPS.WEXPSVR

	CONNECTSHIP_ENDICIA.USPS.FIRST
	CONNECTSHIP_ENDICIA.USPS.PRIORITY
	CONNECTSHIP_ENDICIA.USPS.EXPR
	CONNECTSHIP_ENDICIA.USPS.MEDIA
	CONNECTSHIP_ENDICIA.USPS.PARCELPOST
	CONNECTSHIP_ENDICIA.USPS.I_FIRST
	CONNECTSHIP_ENDICIA.USPS.I_PRIORITY
	CONNECTSHIP_ENDICIA.USPS.I_EXPR
	CONNECTSHIP_ENDICIA.USPS.CRITICAL

	*/	