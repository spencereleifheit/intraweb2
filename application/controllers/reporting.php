<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	/*

		Page Description: View for Image Registry

	*/

	set_time_limit(0);

	class Reporting extends CI_Controller{

		var $helpers = array(
			"sqldata", 
			"livesite",
			"lastsevendays"
			);

		var $dbconns = array();

		public function __construct(){

			parent::__construct();

			// Load all Database Connector Helpers
			foreach ($this->helpers as $helper) {

				$this->load->helper($helper);
			}

		}
	
		public function index() {

			
			$this->dbconns[1] = sqldataConnect('Image_Registry');

			$this->load->view('default_header');
			$this->load->view('reporting_view');

		}

		public function trafficReport() {

			$this->dbconns[0] = liveSiteDBConnect();
			$end = NULL;
			$query = 1;

			$time_start_append = " 00:00:00";
			$time_end_append = " 23:59:59";

			$keyword = $this->input->post('keyword');

			$start = $this->input->post('start');
			$end = $this->input->post('end');

			//$start = "08/26/2014";
			//$keyword = "partsology";

			if ($end == NULL || $end == ''){
				$end = $start;
			}

			$start_year = substr($start,6,4);
			$start_month = substr($start,0,2);
			$start_day = substr($start,3,2);

			$end_year = substr($end,6,4);
			$end_month = substr($start,0,2);
			$end_day = substr($start,3,2);

			$begin = $start_year."-".$start_month."-".$start_day.$time_start_append;
			$end = $end_year."-".$end_month."-".$end_day.$time_end_append;

			$bigAssQuery = "SELECT 

							SessionId,
							
							UserId,
							
							CustNo,
							
							IPAddress,
							
							LogTime,
							
							ActivityType,
							
							Referrer,
							
							PageName

							FROM dbo.SiteActivityLOg

							WHERE

								(

								LogTime >= '".$begin."'

								AND 

								LogTime <= '".$end."'

								)

								AND

								RequestURL LIKE '%".$keyword."%'

								AND 

								(

								IPAddress != '216.64.152.77'

								AND

								IPAddress != '70.90.171.40'
								
								AND
								
								IPAddress != '70.90.171.41'
								
								AND
								
								IPAddress != '70.90.171.42'
								
								AND
								
								IPAddress != '70.90.171.43'
								
								AND
								
								IPAddress != '70.90.171.44'
								
								AND
								
								IPAddress != '70.90.171.45'
								
								AND
								
								IPAddress != '70.90.171.46'
								
								AND
								
								IPAddress != '70.90.171.47'

								)

							ORDER BY LogTime ASC
								";


			$stmt = sqlsrv_query($this->dbconns[0], $bigAssQuery);

			$result = array();

			$true = array (
				"status"=> "true",
				"start" => $begin,
				"keyword" => $keyword
				);

			$this->output->set_header("HTTP/1.0 200 OK");
			$this->output->set_header("HTTP/1.1 200 OK");

			$dookie = array();

			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {
				
				$corn = array(
					"sessionid" => $row['SessionId'],
					"userid" => $row['UserId'],
					"custno" => $row['CustNo'],
					"ipaddress" => $row['IPAddress'],
					"logtime" => $row['LogTime'],
					"activitytype" => $row['ActivityType'],
					"referrer" => $row['Referrer'],
					"pagename" => $row['PageName']
				);

				//$joe[$count] = $dookie;
				array_push($dookie, $corn);

				//$dookie = array($sessionid, $userid, $custno, $ipaddress, $activitytype, $referrer, $pagename);

				// $rows[] = $row;
				// print json_encode($rows);

			}

			//var_dump($dookie);

			echo json_encode($dookie);
			// $result = array(
			// 	"status"=>"true",
			// 	"keyword" => $keyword,
			// 	"start" => $start,
			// 	"end" => $end
			// );

		}

	}
