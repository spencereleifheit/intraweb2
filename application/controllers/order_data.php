<?php


class Order_data extends CI_Controller {

	var $helpers = array(
		//"sqldata", 
		"sqldata2", 
		//"livesite",
		"lastsevendays"
		//"devo2", 
		//"devo3", 
		//"intraweb", 
		//"intraweb2",
		//"previewftp"
		);

	var $dbconn;

	public function __construct(){

		parent::__construct();

		// Load all Database Connector Helpers
		foreach ($this->helpers as $helper) {

			$this->load->helper($helper);
		}

	}

	public function sendOrdateEmail(){

		echo "<h2><strong>No new data found within the last 5 days. Sending notification</h2>";

		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

		$to = 'spencer@horizonparts.com'.', ';
		//$to .= 'kelly@horizonparts.com'.', ';
		//$to .= 'scottD@horizonparts.com';
		$subject = 'Notification from Intraweb regarding ordate data';
		
		$message = "
		Hello, <br/><br/>
		This email is to notify you that no new ordate data has populated<br/>
		 in SQLDATA2 <strong>for at least 5 days.</strong>

		 <br/><br/><br/>
		
		 End of Line.
		 <br/><br/>
		 ---Sincerely, Intraweb
		 
		 ";

		mail($to, $subject, $message, $headers);

	}

	public function ordateReport(){

		$time_append = " 00:00:00.000";

		$days = goGetTheLastSevenDays();

		echo "Searching for updated <strong>ordate</strong> data between ".$days[0]." and ".$days[7];

		echo "<br/><br/>";

		echo "Showing results found by day...";

		$results = array();

		foreach ($days as $day) {

			$result = 0;

			$query = "SELECT ordate FROM dbo.sotran
				 	 	WHERE ordate > = '".$day.$time_append."'
				 	 	AND ordate != '2014-10-15 00:00:00'
				  	 ";

			$stmt = sqlsrv_query($this->dbconn, $query);

			if ($stmt == false) {
				die( print_r( sqlsrv_errors(), true));
			}

			$result = 0;
				
			while($row = sqlsrv_fetch_array( $stmt, SQLSRV_FETCH_ASSOC)) {
				//echo $row['ordate']."<br/>";
				$result++;
			}

		echo "<h3>$day </h3>";
		echo $result." result(s) found";

		array_push($results, $result);

		}		

		var_dump($results);

	}



	public function index() {

		$this->dbconn = sqlData2Connect('Data01HSP');

		$this->ordateReport();



		$this->load->view('ordata_view');



	}
	


}