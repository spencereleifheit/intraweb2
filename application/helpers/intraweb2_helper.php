<?php

	/*	============================================================

		Intraweb 2.0

		Authored by: Spencer Leifheit

		File created 8/5/2014

		For use by Horizon Pool and Spa Parts

	============================================================= */


	//	Helper function to initialize Connection to Intraweb 2.0 MySQL Database


	function intraweb2Connect() {

		$server = "hsp-it-01";
		$user = "dookie";
		$pass = "dookie";
		$db = "h_sql_dev";

		$dbconn = mysqli_connect($server, $user, $pass, $db);

		if ($dbconn) {
			//echo "<h2>Connection established to Intraweb2 (".$server.")</h2>";
			return $dbconn;

		}

		else {
		 	die("Unable to Connect to Intraweb2");
		 	return false;
		}

	}
